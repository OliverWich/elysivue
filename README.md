# Elysia with Bun and a Vue.js frontend

## Development
To start the development server run:
```bash
bun serve
```
or
```bash
bun run --filter "*" dev
```

This will build the vue frontend in watch mode and have it be available on the root path (http://localhost:3000/).
Routes handled by Elysia will be handled correctly, everything non-existent will route to the frontend for that router to handle.

Open http://localhost:3000/ with your browser to see the result.
